import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class aa implements Runnable {
    Canvas convas;
    Main2.Snake snake;
    List<Main2.Food> foods;
    List<Main2.Snake> snakes;
    int place;
    List<Main2.obstacle> obstacles;
    int number;
    boolean road;
    Main2.lock Lock;
    DataInputStream is;
    int i;
    List<aa> threads;
    List<Main2.Food> created_foods;
    List<Integer> variable;
    DataOutputStream out;
    List<Integer> current_directions;
    int my_place;
    int count;
    Stage end;
    List<Integer> losing;
    Stage board;
    List<Integer> count2;
    public aa(Canvas convas, Main2.Snake snake, List<Main2.Food> foods, List<Main2.Snake> snakes, int place, List<Main2.obstacle> obstacles, int number, Main2.lock Lock, DataInputStream is, List<aa> threads, List<Main2.Food> created_foods, DataOutputStream out,List<Integer> current_directions,List<Integer> variable,int my_place,Stage end,List<Integer> losing,Stage board,List<Integer> count2)
    {
        this.convas = convas;
        this.snake = snake;
        this.foods = foods;
        this.snakes = snakes;
        this.place = place;
        this.obstacles = obstacles;
        this.number = number;
        this.road =true;
        this.Lock = Lock;
        this.Lock.lock = 0;
        this.is = is;
        this.i = 0;
        this.threads = threads;
        this.created_foods = created_foods;
        this.out = out;
        this.current_directions = current_directions;
        this.variable = variable;
        this.my_place = my_place;
        this.count = 0;
        this.end = end;
        this.losing = losing;
        this.board = board;
        this.count2 = count2;
    }
    public void run() {
        while (true) {
            synchronized (convas) {
                try {
                    i++;
                    System.out.print("");
                    System.out.print("");
                    System.out.print("");
                    play(convas, snake, foods, snakes, place, obstacles);
                    Lock.lock++;
                    if (Lock.lock == number) {
                        Lock.lock = 0;
                        Thread.sleep(100);
                        synchronized (out) {
                            try {
                                out.writeInt(20);
                                //if ((current_directions.get(t) != 2+directions.get(t)) && (current_directions.get(t) != directions.get(t)-2))
                                //current_directions.set(t, directions.get(t));
                                get_directions(current_directions, is);
                                is.readInt();
                            }
                            catch(Exception e)
                            {
                                e.printStackTrace();
                            }

                        }
                        if (i%25 ==0)
                        add_food(convas, foods, created_foods);
                        convas.notifyAll();
                    } else {
                        convas.wait();
                    }
                } catch (InterruptedException E) {
                    E.printStackTrace();
                }
            }
        }
    }
    public synchronized void play(Canvas convas, Main2.Snake snake, List<Main2.Food> foods, List<Main2.Snake> snakes, int place, List<Main2.obstacle> obstacles)  {
        if (road == true) {
            GraphicsContext context = convas.getGraphicsContext2D();
            context.setFill(Color.WHITE);
            context.fillRect(10 * snake.x.get(0), 10 * snake.y.get(0), 10, 10);
            for (int m = 1; m < snake.length; m++) {
                context.setFill(Color.BLACK);
                context.fillRect(10 * snake.x.get(m), 10 * snake.y.get(m), 10, 10);
            }
            for (int n = 0; n < snake.length - 1; n++) {
                snake.x.set(n, snake.x.get(n + 1));
                snake.y.set(n, snake.y.get(n + 1));
            }
            if (current_directions.get(place) == 0) {
                snake.x.set(snake.length - 1, snake.x.get(snake.length - 1) + 1);
            }
            if (current_directions.get(place) == 1) {
                snake.y.set(snake.length - 1, snake.y.get(snake.length - 1) + 1);
            }
            if (current_directions.get(place) == 2) {
                snake.x.set(snake.length - 1, snake.x.get(snake.length - 1) - 1);
            }
            if (current_directions.get(place) == 3) {
                snake.y.set(snake.length - 1, snake.y.get(snake.length - 1) - 1);
            }
            context.setFill(Color.BLACK);
            if(place != my_place)
            context.fillRect(10 * snake.x.get(snake.length - 1), 10 * snake.y.get(snake.length - 1), 10, 10);
            else
                context.fillOval(10 * snake.x.get(snake.length - 1), 10 * snake.y.get(snake.length - 1), 10, 10);
            add_length(foods, snake);
            avoid_from_border(snake, convas, snakes, place);
            avoid_from_obstacle(snake, convas, snakes, place, obstacles);
            avoid_from_snake(convas, snake, snakes, place,threads);
        }
        else{
            count++;
            if (count == 1)
                losing.set(0,losing.get(0)+1);
            if(place == my_place && count == 1)
            {
                new Thread(() -> {
                    Platform.runLater(() -> {
                        end.show();
                    });
                }).start();
            }
            if(losing.get(0) == number && count2.get(0) == 0) {
                count2.set(0,count2.get(0)+1);
                new Thread(() -> {
                    Platform.runLater(() -> {
                        count2.set(0,count2.get(0)+1);
                        if (count2.get(0) == 2) {
                            board = create_board(snakes, number);
                        }
                        board.show();
                    });
                }).start();

            }
        }
    }
    public synchronized void add_length(List<Main2.Food> foods, Main2.Snake snake)
    {
        int e =0;
        while (e<foods.size())
        {
            if (foods.get(e).x == snake.x.get(snake.length-1) && foods.get(e).y == snake.y.get(snake.length-1))
            {
                snake.score += foods.get(e).score;
                foods.remove(e);
            }
            e++;
        }
        snake.add(current_directions.get(place));
    }
    public synchronized void avoid_from_border(Main2.Snake snake, Canvas convas, List<Main2.Snake> snakes,int place)
    {
        GraphicsContext context = convas.getGraphicsContext2D();
        if(snake.x.get(snake.length-1) == 0 || snake.y.get(snake.length-1) == 0 || snake.x.get(snake.length-1) == 149 || snake.y.get(snake.length-1) == 79)
        {
            //snakes.remove(place);
            //directions.remove(place);
            for (int xy =0;xy<snake.x.size()-1;xy++)
            {
                context.setFill(Color.WHITE);
                context.fillRect(10*snake.x.get(xy),10*snake.y.get(xy),10,10);
                snake.x.set(xy,2000);
                snake.y.set(xy,2000);
            }
            context.setFill(Color.BLUE);
            context.fillRect(10*snake.x.get(snake.x.size()-1),10*snake.y.get(snake.x.size()-1),10,10);
            snake.x.set((snake.x.size()-1),2000);
            snake.y.set((snake.y.size()-1),2000);
            road = false;
        }
    }
    public synchronized void avoid_from_obstacle(Main2.Snake snake, Canvas convas, List<Main2.Snake> snakes, int place, List<Main2.obstacle> obstacles)
    {
        GraphicsContext context = convas.getGraphicsContext2D();
        for (Main2.obstacle ob : obstacles) {
            if (snake.x.get(snake.length - 1) == ob.x && snake.y.get(snake.length - 1) == ob.y) {
                //snakes.remove(place);
                //directions.remove(place);
                for (int xy = 0; xy < snake.x.size() - 1; xy++) {
                    context.setFill(Color.WHITE);
                    context.fillRect(10 * snake.x.get(xy), 10 * snake.y.get(xy), 10, 10);
                    snake.x.set(xy,2000);
                    snake.y.set(xy,2000);
                }
                context.setFill(Color.DARKGREEN);
                context.fillRect(10 * snake.x.get(snake.x.size() - 1), 10 * snake.y.get(snake.x.size() - 1), 10, 10);
                snake.x.set((snake.x.size()-1),2000);
                snake.y.set((snake.y.size()-1),2000);
                road = false;
            }
        }
    }
    public synchronized void add_food(Canvas convas, List<Main2.Food> foods, List<Main2.Food> created_foods)
    {
        Main2.Food food = new Main2.Food();
        if (variable.get(0) < 2000) {
            food.x = created_foods.get(variable.get(0)).x;
            food.y = created_foods.get(variable.get(0)).y;
            food.score = created_foods.get(variable.get(0)).score;
        }
        else {
            variable.set(0,0);
            food.x = created_foods.get(variable.get(0)).x;
            food.y = created_foods.get(variable.get(0)).y;
            food.score = created_foods.get(variable.get(0)).score;
        }
        GraphicsContext context = convas.getGraphicsContext2D();
        context.setFill(Color.RED);
        context.fillOval(10*food.x, 10*food.y, 10, 10);
        foods.add(food);
        variable.set(0,variable.get(0)+1);
    }
    public synchronized void avoid_from_snake(Canvas convas, Main2.Snake snake, List<Main2.Snake> snakes,int place,List<aa> threads)
    {
        GraphicsContext context = convas.getGraphicsContext2D();
        for (int h=0;h<snakes.size();h++)
        {
            if (h == place)
            {
                for (int section=0;section<snake.x.size();section++)
                {
                    if (snake.x.get(snake.x.size() - 1) == snake.x.get(section) && snake.y.get(snake.x.size() - 1) == snake.y.get(section))
                    {
                        if (section != snake.x.size()-1)
                        {
                            for (int xy = 0; xy < snake.x.size(); xy++) {
                                context.setFill(Color.WHITE);
                                context.fillRect(10 * snake.x.get(xy), 10 * snake.y.get(xy), 10, 10);
                            }
                            road = false;
                        }
                    }
                }
            }
            else
            {
                Main2.Snake sn = snakes.get(h);
                for (int section = 0;section<sn.x.size();section++)
                {
                    if (snake.x.get(snake.x.size() - 1) == sn.x.get(section) && snake.y.get(snake.x.size() - 1) == sn.y.get(section))
                    {
                        if (snake.x.size() < sn.x.size())
                        {
                            for (int xy = 0; xy < snake.x.size()-1; xy++) {
                                context.setFill(Color.WHITE);
                                context.fillRect(10 * snake.x.get(xy), 10 * snake.y.get(xy), 10, 10);
                                snake.x.set(xy,2000);
                                snake.y.set(xy,2000);
                            }
                            snake.x.set(snake.x.size()-1,2000);
                            snake.y.set(snake.x.size()-1,2000);
                            snake.length = 0;
                            road = false;
                            sn.score += snake.score;
                            sn.add(current_directions.get(place));
                        }
                        if(snake.x.size() == sn.x.size())
                        {
                            if(snake.score > sn.score)
                            {
                                for (int xy = 0; xy < sn.x.size(); xy++) {
                                    context.setFill(Color.WHITE);
                                    context.fillRect(10 * sn.x.get(xy), 10 * sn.y.get(xy), 10, 10);
                                    sn.x.set(xy,2000);
                                    sn.y.set(xy,2000);
                                }
                                sn.length = 0;
                                context.setFill(Color.BLACK);
                                context.fillRect(10*snake.x.get(snake.x.size()-1),10*snake.y.get(snake.x.size()-1),10,10);
                                threads.get(h).road = false;
                                snake.score += sn.score;
                                snake.add(current_directions.get(place));
                            }
                            if (snake.score < sn.score)
                            {
                                for (int xy = 0; xy < snake.x.size()-1; xy++) {
                                    context.setFill(Color.WHITE);
                                    context.fillRect(10 * snake.x.get(xy), 10 * snake.y.get(xy), 10, 10);
                                    snake.x.set(xy,2000);
                                    snake.y.set(xy,2000);
                                }
                                snake.x.set(snake.x.size()-1,2000);
                                snake.y.set(snake.x.size()-1,2000);
                                snake.length = 0;
                                road = false;
                                sn.score += snake.score;
                                sn.add(current_directions.get(place));
                            }
                            if (snake.score == sn.score)
                            {
                                if(snake.i < sn.i)
                                {
                                    for (int xy = 0; xy < snake.x.size()-1; xy++) {
                                        context.setFill(Color.WHITE);
                                        context.fillRect(10 * snake.x.get(xy), 10 * snake.y.get(xy), 10, 10);
                                        snake.x.set(xy,2000);
                                        snake.y.set(xy,2000);
                                    }
                                    snake.x.set(snake.x.size()-1,2000);
                                    snake.y.set(snake.x.size()-1,2000);
                                    snake.length = 0;
                                    road = false;
                                    sn.score += snake.score;
                                    sn.add(current_directions.get(place));
                                }
                                else
                                {
                                    for (int xy = 0; xy < sn.x.size(); xy++) {
                                        context.setFill(Color.WHITE);
                                        context.fillRect(10 * sn.x.get(xy), 10 * sn.y.get(xy), 10, 10);
                                        sn.x.set(xy,2000);
                                        sn.y.set(xy,2000);
                                    }
                                    sn.length = 0;
                                    context.setFill(Color.BLACK);
                                    context.fillRect(10*snake.x.get(snake.x.size()-1),10*snake.y.get(snake.x.size()-1),10,10);
                                    threads.get(h).road = false;
                                    snake.score += sn.score;
                                    snake.add(current_directions.get(place));
                                }
                            }
                        }
                        if (snake.x.size() > sn.x.size())
                        {
                            for (int xy = 0; xy < sn.x.size(); xy++) {
                                context.setFill(Color.WHITE);
                                context.fillRect(10 * sn.x.get(xy), 10 * sn.y.get(xy), 10, 10);
                                sn.x.set(xy,2000);
                                sn.y.set(xy,2000);
                            }
                            sn.length = 0;
                            context.setFill(Color.BLACK);
                            context.fillRect(10*snake.x.get(snake.x.size()-1),10*snake.y.get(snake.x.size()-1),10,10);
                            threads.get(h).road = false;
                            snake.score += sn.score;
                            snake.add(current_directions.get(place));
                        }
                    }
                }
            }
        }
    }
    public synchronized void get_directions(List<Integer> directions,DataInputStream is)
    {
        for(int t=0;t<directions.size();t++)
        {
            try {
                int dir = is.readInt();
                if ((directions.get(t) != 2+dir) && (directions.get(t) != dir-2))
                directions.set(t, dir);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }
    public Stage create_board(List<Main2.Snake> snake, int number)
    {
        VBox vbox = new VBox();
        for (int r=0;r<number;r++)
        {
            HBox hbox = new HBox();
            Label name = new Label(snake.get(r).name+"   ");
            Label score = new Label(String.format("%d",snake.get(r).score));
            hbox.getChildren().addAll(name,score);
            vbox.getChildren().add(hbox);
        }
        Scene scene = new Scene(vbox);
        Stage stage = new Stage();
        stage.setScene(scene);
        return stage;
    }
}

