import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Main extends Application {
    String iptext;
    int portnumber;
    String name;
    DataInputStream is;
    public void start(Stage primaryStage) {
        List<String> names = new ArrayList<>();
        BorderPane root = new BorderPane();
        Button button = new Button("OK");
        TextField ip = new TextField();
        TextField port = new TextField();
        TextField take_name = new TextField();
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, mouse->
        {
                try {
                    iptext = ip.getText();
                    portnumber = Integer.parseInt(port.getText());
                    name = take_name.getText();
                    Socket s = new Socket(iptext, portnumber);
                    is = connection(iptext, portnumber,s);
                    DataOutputStream out = new DataOutputStream(s.getOutputStream());
                    primaryStage.close();
                    Main2 mainn = new Main2();
                    int number = is.readInt();
                    for(int n =0;n<number;n++)
                    {
                        names.add("");
                    }
                    int my_place = is.readInt();
                    out.writeUTF(name);
                    for(int d = 0;d<number;d++)
                    {
                        int pl = is.readInt();
                        String nm = is.readUTF();
                        names.set(pl,nm);
                    }
                    Stage finish = final_Stage();
                    Stage stage = mainn.main(is, number,out,my_place,name,finish,names);
                    stage.show();
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
        });
        root.setTop(createtop(ip, port,take_name));
        root.setBottom(createbottom(button));
        root.setPadding(new Insets(15));
        Scene scene = new Scene(root, 600, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public Parent createtop(TextField ip, TextField port,TextField take_name) {
        VBox vbox = new VBox();
        vbox.setSpacing(10);
        ip.setPromptText("enter the server ip");
        ip.setMaxWidth(500);
        port.setPromptText("enter the server port");
        port.setMaxWidth(500);
        take_name.setPromptText("enter your name");
        take_name.setMaxWidth(500);
        Label ip_label = new Label("ip:  ");
        Label port_label = new Label("port:  ");
        Label name_label = new Label("name:  ");
        HBox ip_box = new HBox();
        ip_box.setHgrow(ip, Priority.ALWAYS);
        HBox port_box = new HBox();
        port_box.setHgrow(port, Priority.ALWAYS);
        HBox name_box = new HBox();
        name_box.setHgrow(take_name,Priority.ALWAYS);
        ip_box.getChildren().add(ip_label);
        ip_box.getChildren().add(ip);
        port_box.getChildren().add(port_label);
        port_box.getChildren().add(port);
        name_box.getChildren().add(name_label);
        name_box.getChildren().add(take_name);
        vbox.getChildren().addAll(ip_box, port_box,name_box);
        return vbox;
    }

    public Parent createbottom(Button button) {
        HBox hbox = new HBox();
        button.setPrefWidth(80);
        hbox.setAlignment(Pos.BOTTOM_RIGHT);
        hbox.getChildren().add(button);
        return hbox;
    }

    public static Stage create_second() {
        Stage second = new Stage();
        HBox hbox = new HBox();
        Label label = new Label();
        label.setText("please wait for other players");
        label.setFont(Font.font(20));
        hbox.setAlignment(Pos.CENTER);
        hbox.getChildren().add(label);
        Scene scene = new Scene(hbox, 300, 100);
        second.setScene(scene);
        second.initModality(Modality.APPLICATION_MODAL);
        return second;
    }

    public static DataInputStream connection(String iptext,int portnumber,Socket s) throws Exception
    {
            Stage second = create_second();
            InputStream ss =s.getInputStream();
            DataInputStream in = new DataInputStream(ss);
            second.initStyle(StageStyle.UTILITY);
            second.show();
            boolean bool = true;
            while(bool)
            {
                if (in.readInt() == 1) {
                    bool = false;
                }
            }
            second.close();
            return in;
    }
    public Stage final_Stage()
    {
        Label game_over = new Label("Game Over");
        game_over.setFont(Font.font(20));
        HBox finish = new HBox();
        finish.getChildren().add(game_over);
        Scene scene = new Scene(finish);
        Stage my_Stage = new Stage();
        my_Stage.setScene(scene);
        return my_Stage;
    }
    public static void main(String[] args) {
        launch(args);
    }
}
