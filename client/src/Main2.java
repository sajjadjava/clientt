import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import jdk.internal.org.objectweb.asm.tree.TryCatchBlockNode;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.net.*;
import java.util.Random;
import java.util.Scanner;


public class Main2 {
    List<obstacle> obstacles = new ArrayList<>();
    lock Lock = new lock();
    List<Food> created_foods = new ArrayList<>();
    int dir;
    List<Integer> current_directions = new ArrayList<>();
    List<Integer> variable = new ArrayList<>();
    String name;
    List<Integer> losing = new ArrayList<>();
    List<Integer> count2 = new ArrayList<>();
    public Stage main(DataInputStream is, int number,DataOutputStream out,int my_place,String name,Stage finish,List<String> names)
    {
        losing.add(0);
        count2.add(0);
        this.name = name;
        System.out.print(name);
        synchronized (variable) {
            variable.add(0);
        }
        for (int z=0;z<number;z++)
        {
            synchronized (current_directions) {
                current_directions.add(0);
            }
        }
        List<Snake> snake = new ArrayList<>();
        List<Food> foods = new ArrayList<>();
        Stage board = new Stage();
        for (int j=0;j<number;j++)
        {
            Snake sn = new Snake(j,names.get(j));
            snake.add(sn);
        }
        Canvas convas = new Canvas();
        convas.setWidth(1500);
        convas.setHeight(800);
        synchronized (convas) {
            add_obstacle(obstacles, convas, is);
        }
        synchronized (convas) {
            add_border(convas);
        }
        get_foods(is);
        List<aa> threads = new ArrayList<>();
        for (int r = 0; r < snake.size(); r++) {
            threads.add(new aa(convas, snake.get(r),foods,snake,r,obstacles,number,Lock,is,threads,created_foods,out,current_directions,variable,my_place,finish,losing,board,count2));
        }
        for (int q=0;q<snake.size();q++)
        {
            Runnable runn = threads.get(q);
            Thread thread = new Thread(runn);
            thread.start();
        }
        Group group = new Group();
        group.getChildren().add(convas);
        Scene scene = new Scene(group,1500,800);
        Stage primaryStage = new Stage();
        primaryStage.setOnCloseRequest(event->
        {
            synchronized (out) {
                try
                {
                out.writeInt(30);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        scene.addEventFilter(KeyEvent.KEY_PRESSED,key->
        {
            synchronized (out) {
                if (key.getCode() == KeyCode.RIGHT) {
                    if (dir != 2) {
                        try {
                            out.writeInt(10);
                            dir = 0;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (key.getCode() == KeyCode.DOWN) {
                    if (dir != 3) {
                        try {
                            out.writeInt(11);
                            dir = 1;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (key.getCode() == KeyCode.LEFT) {
                    if (dir != 0) {
                        try {
                            out.writeInt(12);
                            dir = 2;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (key.getCode() == KeyCode.UP) {
                    if (dir != 1) {
                        try {
                            out.writeInt(13);
                            dir = 3;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }
        });
        primaryStage.setScene(scene);
        return primaryStage;
    }

    public void add_border(Canvas convas)
    {
        GraphicsContext context = convas.getGraphicsContext2D();
        for (int c=0;c<150;c++)
        {
            context.setFill(Color.BLUE);
            context.fillRect(10*c,0,10,10);
            context.setFill(Color.BLUE);
            context.fillRect(10*c,790,10,10);
        }
        for (int u=0;u<80;u++)
        {
            context.setFill(Color.BLUE);
            context.fillRect(0,10*u,10,10);
            context.setFill(Color.BLUE);
            context.fillRect(1490,10*u,10,10);
        }
    }
    public synchronized void add_obstacle(List<obstacle> obstacles,Canvas convas,DataInputStream is)
    {
        try {
            for (int h = 0; h < 20; h++) {
                obstacle ob = new obstacle();
                ob.x = is.readInt();
                ob.y = is.readInt();
                obstacles.add(ob);
            }
            for (obstacle ob : obstacles) {
                GraphicsContext context = convas.getGraphicsContext2D();
                context.setFill(Color.DARKGREEN);
                context.fillRect(ob.x * 10, ob.y * 10, 10, 10);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public synchronized void get_foods(DataInputStream is)
    {
        for (int i =0;i<2000;i++)
        {
            try {
                Food food = new Food();
                food.x = is.readInt();
                food.y = is.readInt();
                food.score = is.readInt();
                created_foods.add(food);
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
        }
    }
    class Snake
    {
        int i;
        int score ;
        int length;
        String name;
        List<Integer> x = new ArrayList<>();
        List<Integer> y = new ArrayList<>();
        public Snake(int i,String name)
        {
            length =3;
            score = 0;
            this.name = name;
            this.i=i;
            for (int l=0;l<length;l++)
            {
                x.add(5+l);
            }
            for (int p=0;p<length;p++)
            {
                y.add((10*i)+2);
            }
        }
        public void add(int dir)
        {
            if (length - 2 < score/90)
            {
                int len = length;
                for (int w=0;w<(score/90)-(len -2);w++)
                {
                    length++;
                    if (dir == 0)
                    {
                        int X = x.get(0);
                        x.add(0,X-1);
                        int Y = y.get(0);
                        y.add(0,Y);
                    }
                    if (dir == 1)
                    {
                        int X = x.get(0);
                        x.add(0,X);
                        int Y = y.get(0);
                        y.add(0,Y-1);
                    }
                    if (dir == 2)
                    {
                        int X = x.get(0);
                        x.add(0,X+1);
                        int Y = y.get(0);
                        y.add(0,Y);
                    }
                    if (dir == 3)
                    {
                        int X = x.get(0);
                        x.add(0,X);
                        int Y = y.get(0);
                        y.add(0,Y+1);
                    }
                }
            }
        }
    }
    static class Food
    {
        int x;
        int y;
        int score;
    }
    class obstacle
    {
        int x;
        int y;
    }
    class lock
    {
        int lock;
    }
}
